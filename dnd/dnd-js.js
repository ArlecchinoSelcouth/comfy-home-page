let getGalleryData = async function() {
  let response = await fetch("/data/dnd-gallery.json");
  let data = await response.json();
  showGallery(data);
}

let showGallery = async function(data) {
  let projects = data.projects;
  let gallery = document.querySelector(".dnd-gallery");
  let template = document.querySelector("#dnd-latest");
  console.log(projects);
  for(element of projects) {
    let newProject = template.content.cloneNode(true);
    newProject.querySelector(".dnd-latest-link").href = element.siteUrl;
    console.log(newProject.querySelector(".dnd-"))
    newProject.querySelector(".dnd-latest-image").src = element.imageUrl;
    newProject.querySelector(".dnd-latest-text").innerHTML = element.text;
    gallery.appendChild(newProject);
  }
}

getGalleryData();
